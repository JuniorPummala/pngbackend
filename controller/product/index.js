const jwt = require("jsonwebtoken");
const expressJwt = require("express-jwt");
const config = require("./../../config");
const { validationResult } = require("express-validator/check");
const bcrypt = require("bcrypt");
const nodemailer = require("nodemailer");
var moment = require("moment-timezone");
const validator = require("validator");
const { resp } = require("../../helper/response");
const connection = require("express-myconnection");
const authCheck = require("../../helper/getId");
require("dotenv").config();

const aws = require("aws-sdk");
const multer = require("multer");
const multerS3 = require("multer-s3");

const spacesEndpoint = new aws.Endpoint(process.env.BUCKET_ENDPOINT);
const s3 = new aws.S3({
  endpoint: spacesEndpoint,
  accessKeyId: process.env.BUCKET_KEY,
  secretAccessKey: process.env.BUCKET_SECREET,
});

const uploadProduct = multer({
  storage: multerS3({
    s3: s3,
    bucket: process.env.BUCKET,
    key: function (req, file, cb) {
      cb(
        null,
        "product" + "/" + new Date().getTime() + "_" + file.originalname
      );
    },
  }),
}).single("image");

const uploadBanner = multer({
  storage: multerS3({
    s3: s3,
    bucket: process.env.BUCKET,
    key: function (req, file, cb) {
      cb(null, "Banner" + "/" + new Date().getTime() + "_" + file.originalname);
    },
  }),
}).single("image");

exports.productUpload = async (req, res, next) => {
  uploadProduct(req, res, function (err) {
    if (err) {
      return res.status(400).send(resp(false, err));
    } else {
      let path = req.file.key;
      return res.status(200).send(resp(true, path));
    }
  });
};

exports.bannerUpload = async (req, res, next) => {
  uploadBanner(req, res, function (err) {
    if (err) {
      return res.status(400).send(resp(false, err));
    } else {
      let path = req.file.key;
      return res.status(200).send(resp(true, path));
    }
  });
};

exports.updateBanner = async (req, res, next) => {
  try {
    let { body } = req;
    let id = body.bannerId;
    let path = body.path;
    let oldPath = body.oldPath;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "UPDATE `commerce-liff`.`banner` SET img = ?  WHERE `idbanner` =  ? ";
      await connection.query(sql, [path, id], async (err, result) => {
        if (err) {
          next(err);
        } else {
          const param = {
            Bucket: process.env.BUCKET,
            Key: oldPath,
          };
          s3.deleteObject(param, function (err, data) {
          
            if (err) {
              return res.status(200).send(resp(false, err));
            } else {
              return res
                .status(200)
                .send(resp(true, { result: result, data: data }));
            }
          });
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getAllProduct = async (req, res, next) => {
  try {
    let { body } = req;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql = "SELECT * FROM `commerce-liff`.products; ";
      await connection.query(sql, [], (err, result) => {
        if (err) {
          return next(err);
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getDetailProduct = async (req, res, next) => {
  try {
    let { body } = req;
    let idProduct = body.idProduct;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql = "SELECT * FROM `commerce-liff`.products where idproducts = ?;";
      await connection.query(sql, [idProduct], (err, result) => {
        if (err) return next(err);
        if (result.length > 0) {
          return res.status(200).send(resp(true, result));
        } else {
          return res.status(200).send(resp(false, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getAllBanner = async (req, res, next) => {
  try {
    let { body } = req;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql = "SELECT * FROM `commerce-liff`.banner; ";
      await connection.query(sql, [], (err, result) => {
        if (err) return next(err);
        if (result.length > 0) {
          return res.status(200).send(resp(true, result));
        } else {
          return res.status(200).send(resp(false, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getAllRecommend = async (req, res, next) => {
  try {
    let { body } = req;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql = "SELECT * FROM `commerce-liff`.products where recommend = 1;";
      await connection.query(sql, [], (err, result) => {
        if (err) return next(err);
        if (result.length > 0) {
          return res.status(200).send(resp(true, result));
        } else {
          return res.status(200).send(resp(false, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.addRecommend = async (req, res, next) => {
  try {
    let { body } = req;
    let productId = body.productId;

    await req.getConnection(async (err, connection) => {
      let sql = "INSERT INTO `commerce-liff`.`recommend` (product) VALUES (?)";
      await connection(sql, [productId], async (err, result) => {
        if (err) {
          res.status(400).send(resp(false, result));
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.deleteRecommend = async (req, res, next) => {
  try {
    let { body } = req;
    let idrecommend = body.idrecommend;

    await req.getConnection(async (err, connection) => {
      let sql =
        "DELETE FROM `commerce-liff`.`recommend` WHERE idrecommend = ? ;";
      await connection(sql, [idrecommend], async (err, result) => {
        if (err) {
          res.status(400).send(resp(false, result));
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.inSertProduct = async (req, res, next) => {
  try {
    let { body } = req;
    let name = body.name;
    let price = body.price;
    let detail = body.detail;
    let stock = body.stock;
    let img = body.img;

    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "INSERT INTO `commerce-liff`.`products` (name, detail, price, inStock, img) VALUES (?,?,?,?,?)";
      await connection.query(
        sql,
        [name, detail, price, stock, img],
        async (err, result) => {
          if (err) {
            res.status(400).send(resp(false, result));
          } else {
            return res.status(200).send(resp(true, result));
          }
        }
      );
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.updateProduct = async (req, res, next) => {
  try {
    let { body } = req;
    let name = body.name;
    let price = body.price;
    let detail = body.detail;
    let img = body.img;
    let idproducts = body.idproducts;
    let hidden = body.hidden;
    let recommend = body.recommend;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "UPDATE `commerce-liff`.`products` SET name = ? , detail = ? , price = ?, img = ?, hidden = ?, recommend = ? WHERE idproducts = ?;";
      await connection.query(
        sql,
        [name, detail, price, img, hidden, recommend, idproducts],
        async (err, result) => {
          if (err) {
            res.status(400).send(resp(false, result));
          } else {
            return res.status(200).send(resp(true, result));
          }
        }
      );
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.updateStock = async (req, res, next) => {
  try {
    let { body } = req;
    let stock = body.stock;
    let idproducts = body.idproducts;

    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "UPDATE `commerce-liff`.`products` SET inStock = (inStock+?) WHERE idproducts = ?;";
      await connection.query(sql, [stock, idproducts], async (err, result) => {
        if (err) {
          res.status(400).send(resp(false, result));
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getImage = async (req, res, next) => {
  try {
    let { body } = req;
    let img = body.img;

    let image = req.params.image;
    let getParams = {
      Bucket: process.env.BUCKET,
      Key: `${req.params.product}/${image}`,
    };

    s3.getObject(getParams, async function (err, data) {
      if (err) {
        if (err) return next(err);
      } else {
        res.writeHead(200, {
          "Content-Type": "image/png",
        });
        res.write(data.Body, "binary");
        res.end(null, "binary");
      }
    });
  } catch (err) {
    res.status(500).json({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.inSertBanner = async (req, res, next) => {
  try {
    let { body } = req;
    let name = body.name;
    let price = body.price;
    let detail = body.detail;
    let stock = body.stock;
    let img = body.img;

    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "INSERT INTO `commerce-liff`.`products` (name, detail, price, inStock, img) VALUES (?,?,?,?,?)";
      await connection.query(
        sql,
        [name, detail, price, stock, img],
        async (err, result) => {
          if (err) {
            res.status(400).send(resp(false, result));
          } else {
            return res.status(200).send(resp(true, result));
          }
        }
      );
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getBanner = async (req, res, next) => {
  try {
    let { body } = req;
    let uId = body.uId;

    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql = "SELECT * FROM `commerce-liff`.banner;";
      await connection.query(sql, [uId], (err, result) => {
        if (err) {
          return res.status(200).send(resp(true, result));
        } else {
          return res.status(200).send(resp(false, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.addOrder = async (req, res, next) => {
  try {
    let { body } = req;
    let status = body.status;
    let customerID = body.customerID;
    let products = body.products;
    let OrderId = undefined;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "INSERT INTO `commerce-liff`.`orders` (`customerID`) VALUES (?)";
      await connection.query(sql, [customerID], async (err, result) => {
        if (err) {
          return next(err);
        } else {
          OrderId = result.insertId;
          let OrderDetail =
            "INSERT INTO `commerce-liff`.`orderDetail` (`amount`,`product`,`order`) VALUES ";
          if (result.insertId) {
            await products.forEach(async (data, i) => {
              let lastChar = ",";
              if (i == products.length - 1) {
                lastChar = ";";
              }
              let values =
                "(" +
                `${data.quantity}` +
                "," +
                `${data.idproducts}` +
                "," +
                `${result.insertId}` +
                ")" +
                lastChar;
              OrderDetail = OrderDetail + values;
            });
            await connection.query(OrderDetail, [], async (err, result2) => {
              if (err) {
                return next(err);
              } else {
                let getNO =
                  "SELECT * FROM `commerce-liff`.orders where idorders = ? ;";
                await connection.query(
                  getNO,
                  [OrderId],
                  async (err, result3) => {
                    if (err) {
                      return next(err);
                    } else {
                      return res.status(200).json(resp(true, result3));
                    }
                  }
                );
              }
            });
          } else {
            return res.status(200).json(resp(false, result));
          }
        }
      });
    });
  } catch (err) {
    res.status(500).json({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.updateAddress = async (req, res, next) => {
  try {
    let { body } = req;
    let idUser = body.idUser;
    let address = body.address;

    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "UPDATE `commerce-liff`.`user` SET  address = ? WHERE iduser = ? ;";
      await connection.query(sql, [address, idUser], async (err, result) => {
        if (err) {
          res.status(400).send(resp(false, result));
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getOrder = async (req, res, next) => {
  try {
    let { body } = req;
    await req,
      getConnection(async (err, connection) => {
        if (err) return next(err);
        let sql = "SELECT * FROM `commerce-liff`.orders;";
        await connection.query(sql, [], (err, result) => {
          if (err) {
            return res.status(200).send(resp(true, result));
          } else {
            return res.status(200).send(resp(false, result));
          }
        });
      });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getAllOrders = async (req, res, next) => {
  try {
    let { body } = req;
    let token = body.token;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "SELECT *, (case when status = 1 then 'รอตรวจสอบ' when status = 2 then 'รอการจัดส่ง'  when status = 3 then 'จัดส่งเเล้ว' when status = 0 then 'ยกเลิก' end) AS orderStatus FROM `commerce-liff`.orders INNER JOIN (SELECT iduser, lineName, phone, address FROM `commerce-liff`.user )  User ON User.iduser = orders.customerID ORDER BY timestamp ASC ;";
      await connection.query(sql, [], (err, result) => {
        if (err) {
          return next(err);
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getAllOrdersDeliver = async (req, res, next) => {
  try {
    let { body } = req;
    let token = body.token;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "SELECT *, (case when status = 1 then 'รอตรวจสอบ' when status = 2 then 'รอการจัดส่ง'  when status = 3 then 'จัดส่งเเล้ว' when status = 0 then 'ยกเลิก' end) AS orderStatus FROM `commerce-liff`.orders INNER JOIN (SELECT iduser, lineName, phone, address FROM `commerce-liff`.user )  User ON User.iduser = orders.customerID where status = 2 or status = 3 ORDER BY timestamp ASC ;";
      await connection.query(sql, [], (err, result) => {
        if (err) {
          return next(err);
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getDailyOrders = async (req, res, next) => {
  try {
    let { body } = req;
    let date = body.date;
    let to = body.to;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "SELECT *, (case when status = 1 then 'รอตรวจสอบ' when status = 2 then 'รอการจัดส่ง'  when status = 3 then 'จัดส่งเเล้ว' when status = 0 then 'ยกเลิก' end) AS orderStatus \
      FROM `commerce-liff`.orders \
      INNER JOIN (SELECT iduser, lineName, phone, address FROM `commerce-liff`.user )  User ON User.iduser = orders.customerID \
      where orders.date_create between ?  and ? order by timestamp ASC; ";
      await connection.query(sql, [date, to], (err, result) => {
        if (err) {
          return next(err);
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getFillterOrders = async (req, res, next) => {
  try {
    let { body } = req;
    let date = body.date;
    let to = body.to;
    let status = body.status;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);

      let sqlStatus = null;

      if (status === "all") {
        sqlStatus = ` status = 0 or status = 1 or status = 2 or status = 3 and `;
      } else {
        sqlStatus = ` status = ${status} and `;
      }

      let sql =
        "SELECT *, (case when status = 1 then 'รอตรวจสอบ' when status = 2 then 'รอการจัดส่ง'  when status = 3 then 'จัดส่งเเล้ว' when status = 0 then 'ยกเลิก' end) AS orderStatus \
      FROM `commerce-liff`.orders \
      INNER JOIN (SELECT iduser, lineName, phone, address FROM `commerce-liff`.user )  User ON User.iduser = orders.customerID \
      where " +
        sqlStatus +
        " orders.date_create between '" +
        date +
        "' AND '" +
        to +
        "' order by timestamp ASC; ";

      await connection.query(sql, [date, to], (err, result) => {
        if (err) {
          return next(err);
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getOrderDetail = async (req, res, next) => {
  try {
    let { body } = req;
    let NO = body.NO;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "SELECT idorders, NO,  (case when status = 1 then 'รอตรวจสอบ' when status = 2 then 'รอการจัดส่ง'  when status = 3 then 'จัดส่งเเล้ว' when status = 0 then 'ยกเลิก' end) AS status , product, \
      product_name, img, price, amount ,inStock, amount*price as total_price , lineName, phone, address  ,date_create \
       FROM `commerce-liff`.orders \
      INNER JOIN (SELECT idorderDetail, amount , product, orderDetail.order as OrderId FROM `commerce-liff`.orderDetail)  Detail ON Detail.OrderId = orders.idorders \
      INNER JOIN (SELECT iduser, lineName, phone, address FROM `commerce-liff`.user )  User ON User.iduser = orders.customerID \
      INNER JOIN (SELECT idproducts, name as product_name, price ,inStock, img FROM `commerce-liff`.products)  Product ON Product.idproducts = Detail.product \
      where NO = ? ; ";
      await connection.query(sql, [NO], (err, result) => {
        if (err) {
          return next(err);
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.approveStatusOrder = async (req, res, next) => {
  try {
    let { body } = req;
    let idorders = body.idorders;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "UPDATE `commerce-liff`.`orders` SET  status = 2 WHERE idorders = ? ;";
      await connection.query(sql, [idorders], (err, result) => {
        if (err) {
          return next(err);
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.deliverStatusOrder = async (req, res, next) => {
  try {
    let { body } = req;
    let idorders = body.idorders;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "UPDATE `commerce-liff`.`orders` SET  status = 3 WHERE idorders = ? ;";
      await connection.query(sql, [idorders], (err, result) => {
        if (err) {
          return next(err);
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.rejectStatusOrder = async (req, res, next) => {
  try {
    let { body } = req;
    let idorders = body.idorders;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "UPDATE `commerce-liff`.`orders` SET  status = 0 WHERE idorders = ? ;";
      await connection.query(sql, [idorders], (err, result) => {
        if (err) {
          return next(err);
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getLotIdByProduct = async (req, res, next) => {
  try {
    let { body } = req;
    let idProduct = body.idProduct;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql = "SELECT * FROM `commerce-liff`.stock where product = ? ;";
      await connection.query(sql, [idProduct], (err, result) => {
        if (err) {
          return res.status(200).send(resp(false, result));
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.insertStockOut = async (req, res, next) => {
  try {
    let { body } = req;
    let idLot = body.idLot;
    let product = body.product;
    let detail = body.detail;
    let amount = body.amount;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "INSERT INTO `commerce-liff`.outStock (idLot, product, detail, amount) VALUES (?,?, ?, ?)";
      await connection.query(
        sql,
        [idLot, product, detail, amount],
        (err, result) => {
          if (err) {
            return res.status(200).send(resp(false, result));
          } else {
            return res.status(200).send(resp(true, result));
          }
        }
      );
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.updateBigstock = async (req, res, next) => {
  try {
    let { body } = req;
    let idProduct = body.idProduct;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "SELECT * FROM `commerce-liff`.stock where product = ? order by timestamp asc;";
      await connection.query(sql, [idProduct], async (err, results) => {
        if (err) {
          return res
            .status(200)
            .send(resp(false, "ไม่พบ Stock สำหรับสินค้านี้"));
        } else {
          let amountLot = 0;

          for (const i of results) {
            amountLot = amountLot + i.amount;
          }
          let updateSQL =
            "UPDATE `commerce-liff`.`products` SET inStock = ? WHERE `idproducts` = ? ;";
          await connection.query(
            updateSQL,
            [amountLot, idProduct],
            (err, results) => {
              if (err) {
                return res.status(200).send(resp(false, err));
              } else {
                return res.status(200).send(resp(true, results));
              }
            }
          );
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.stockOutWhenBuy = async (req, res, next) => {
  try {
    let { body } = req;
    let idProduct = body.idProduct;
    let amount = body.amount;

    await req.getConnection(async (err, connection) => {
      if (err) return next(err);

      async function getAllStock(idProduct) {
        let sql =
          "SELECT * FROM `commerce-liff`.stock where product = ? order by timestamp asc;";
        return new Promise((resolve) => {
          connection.query(sql, [idProduct], (err, results) => {
            if (err) return next(err);
            resolve(results);
          });
        });
      }
      async function getFirstStock(id) {
        let sql = "SELECT * FROM `commerce-liff`.stock where idstock = ? ;";
        return new Promise((resolve) => {
          connection.query(sql, [id, idProduct], (err, results) => {
            if (err) return next(err);
            resolve(results);
          });
        });
      }
      async function updateStockFIFO(amount, realtimeStock, id) {
        let sql =
          "UPDATE `commerce-liff`.`stock` set amount = ? , realtimeStock = ? WHERE idstock = ?;";
        return new Promise((resolve) => {
          connection.query(sql, [amount, realtimeStock, id], (err, results) => {
            if (err) return next(err);
            resolve(results);
          });
        });
      }

      async function asyncCall() {
        const allStock = await getAllStock(idProduct);
        let bufferAmount = amount;
        for (const i of allStock) {
          const firstStock = await getFirstStock(i.idstock);
          if (firstStock[0]?.amount >= bufferAmount) {
            let buffer = firstStock[0]?.amount - bufferAmount;
            await updateStockFIFO(buffer, buffer, firstStock[0].idstock);
            break;
          } else if (firstStock[0]?.amount <= bufferAmount) {
            let buffer = bufferAmount - firstStock[0]?.amount;
            bufferAmount = buffer;
            await updateStockFIFO(0, 0, firstStock[0].idstock);
          }
        }
      }

      asyncCall().then((data) => {
        return res.status(200).send(resp(true, "อัพเดท stock สำเร็จ"));
      });

    
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.updateStockIn = async (req, res, next) => {
  try {
    let { body } = req;
    let idProduct = body.idProduct;
    let lotname = body.lotname;
    let expire = body.expire;
    let amount = body.amount;
    let status = body.status;
    let oldStock = body.oldStock;
    let realTimeStock = 0;
    if (status === 1) {
      realTimeStock = parseInt(oldStock) + parseInt(amount);
    } else {
      realTimeStock = parseInt(oldStock) - parseInt(amount);
    }
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "INSERT INTO `commerce-liff`.stock (lotname, expire, product, amount, status, realtimeStock) VALUES (?,?, ?, ?, ?, ?)";
      await connection.query(
        sql,
        [lotname, expire, idProduct, amount, status, realTimeStock],
        (err, result) => {
          if (err) {
            return res.status(200).send(resp(false, result));
          } else {
            return res.status(200).send(resp(true, result));
          }
        }
      );
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getStockById = async (req, res, next) => {
  try {
    let { body } = req;
    let idProduct = body.idProduct;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "SELECT idstock,product,name,amount,(case when status = 1 then 'นำเข้า' when status = 0 then 'นำออก' end) AS status, inStock,realtimeStock, stock.timestamp FROM `commerce-liff`.stock \
        inner join (SELECT * FROM `commerce-liff`.products) product ON product.idproducts = stock.product where product =  ? ; ";
      await connection.query(sql, [idProduct], (err, result) => {
        if (err) {
          return res.status(200).send(resp(false, result));
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getAllStock = async (req, res, next) => {
  try {
    let { body } = req;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "SELECT idstock,product,name,amount,(case when status = 1 then 'นำเข้า' when status = 0 then 'นำออก' end) AS status, inStock,realtimeStock, stock.timestamp FROM `commerce-liff`.stock \
        inner join (SELECT * FROM `commerce-liff`.products) product ON product.idproducts = stock.product";
      await connection.query(sql, [], (err, result) => {
        if (err) {
          return res.status(200).send(resp(false, result));
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getStockInByProduct = async (req, res, next) => {
  try {
    let { body } = req;
    let idProduct = body.idProduct;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "SELECT * FROM `commerce-liff`.stock where product = ? order by timestamp desc ;";
      await connection.query(sql, [idProduct], (err, result) => {
        if (err) {
          return res.status(200).send(resp(false, result));
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.getStockOutByProduct = async (req, res, next) => {
  try {
    let { body } = req;
    let idProduct = body.idProduct;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql =
        "SELECT * FROM `commerce-liff`.outStock \
      inner join (SELECT idstock, lotname FROM `commerce-liff`.stock) stock ON stock.idstock = outStock.idLot \
      where outStock.product = ? order by timestamp desc;";
      await connection.query(sql, [idProduct], (err, result) => {
        if (err) {
          return res.status(200).send(resp(false, result));
        } else {
          return res.status(200).send(resp(true, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};
// exports.addOrderDetail = async (req, res, next) => {
//   try {
//     let { body } = req;
//   } catch (err) {
//     res.status(500).send({
//       success: "ERROR",
//       message: err.message,
//     });
//   }
// };
