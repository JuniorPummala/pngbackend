const jwt = require("jsonwebtoken");
const expressJwt = require("express-jwt");
const config = require("./../../config");
const bcrypt = require("bcrypt");
var moment = require("moment-timezone");
const validator = require("validator");
const { resp } = require("../../helper/response");
const connection = require("express-myconnection");
const authCheck = require("../../helper/getId");
const { body, validationResult } = require("express-validator");
require("dotenv").config();

const aws = require("aws-sdk");
const multer = require("multer");
const multerS3 = require("multer-s3");

const { default: isEmail } = require("validator/lib/isEmail");

exports.adminSignup = async (req, res, next) => {
  try {
    let { body } = req;
    let name = body.name;
    let email = body.email;
    let password = body.password;
    let phone = body.phone;
    let confirmPassword = body.confirmPassword;

    const errors = await validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).send(resp(false, errors.array()));
    } else {
      await req.getConnection(async (err, connection) => {
        if (err) return next(err);
        let checkUser =
          "SELECT * FROM `commerce-liff`.admin where phone = ? or email = ? ;";
        await connection.query(
          checkUser,
          [phone, email],
          async (err, userCheck) => {
            if (err) {
              return next(err);
            }
            if (userCheck.length > 0) {
              return res
                .status(200)
                .send(
                  resp(
                    false,
                    "ที่อยู่ email หรือ เบอร์มือถือนี้ถูกใช้งานไปแล้ว"
                  )
                );
            } else {
              if (validatePhoneNumber(phone) === true && phone.length == 10) {
                let sql =
                  "INSERT INTO `commerce-liff`.`admin` (name,email,password, phone) VALUES (?,  ? , ?, ?) ;";
                const passwordHash = bcrypt.hashSync(password, 10);
                await connection.query(
                  sql,
                  [name, email, passwordHash, phone],
                  async (err, result) => {
                    if (err) {
                      return next(err);
                    } else {
                      return res.status(200).send(resp(true, result));
                    }
                  }
                );
              } else {
                return res
                  .status(200)
                  .send(resp(false, "Phone Format not true"));
              }
            }
          }
        );
      });
    }
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.checkSignup = async (req, res, next) => {
  try {
    let { body } = req;
    let uId = body.uId;

    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql = "SELECT * FROM `commerce-liff`.user where lineId=? ;";
      await connection.query(sql, [uId], (err, result) => {
        if (err) return next(err);
        if (result.length > 0) {
          return res.status(200).send(resp(true, result));
        } else {
          return res.status(200).send(resp(false, result));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.adminSignin = async (req, res, next) => {
  try {
    let { body } = req;
    let phone = body.phone;
    let password = body.password;

    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      let sql = "SELECT * FROM `commerce-liff`.admin where phone = ? ;";
      await connection.query(sql, [phone], (err, result) => {
        if (err) {
          return next(err);
        } else if (result[0]) {
          const isCorrect = bcrypt.compareSync(password, result[0].password);
          if (isCorrect) {
            const token = jwt.sign(
              {
                id: result[0].idadmin,
                email: result[0].email,
                phone: result[0].phone,
                iat: Math.floor(new Date() / 1000),
              },
              config.secret
            );
            res.cookie("tokenShareExpire", token, {
              expire: new Date() + 9999,
            });
            return res.status(200).send(
              resp(true, {
                token: token,
              })
            );
          } else {
            return res.status(200).send(resp(false, "รหัสผ่านไม่ถูกต้อง"));
          }
        } else {
          return res.status(200).send(resp(false, "ไม่พบบัญชีผู้ใช้"));
        }
      });
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

const validatePhoneNumber = (number) => {
  const isValidPhoneNumber = validator.isMobilePhone(number);
  return isValidPhoneNumber;
};

exports.InsertUser = async (req, res, next) => {
  try {
    let { body } = req;
    let uId = body.uId;
    let phone = body.phone;
    let name = body.name;

    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      if (validatePhoneNumber(phone) === true && phone.length == 10) {
        console.log("emo : ", body.name);
        let sql =
          "INSERT INTO `commerce-liff`.`user` (lineId,lineName,phone) VALUES " +
          `(?,  '${body.name}' , '${phone}') ;`;

        await connection.query(sql, [uId], async (err, result) => {
          if (err) {
            console.log("result : ", result);
            return next(err);
          } else {
            console.log("success : ", result);
            return res.status(200).send(resp(true, result));
          }
        });
      } else {
        return res.status(200).send(resp(false, "Phone Format not true"));
      }
    });
  } catch (err) {
    res.status(500).send({
      success: "ERROR",
      message: err.message,
    });
  }
};

exports.requireSignin = expressJwt({
  secret: config.secret,
  userProperty: "auth",
  algorithms: ["sha1", "RS256", "HS256"],
});
