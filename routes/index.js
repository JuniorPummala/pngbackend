const express = require("express");
const router = express.Router();
const { body, param, check } = require("express-validator");
const {
  checkSignup,
  InsertUser,
  adminSignup,
  requireSignin,
  adminSignin,
} = require("./../controller/auth");

const auth = require("./../helper/verifyToken");
const {
  getAllProduct,
  getAllBanner,
  inSertProduct,
  productUpload,
  bannerUpload,
  addRecommend,
  deleteRecommend,
  updateProduct,
  updateStock,
  getImage,
  addOrder,
  inSertBanner,
  getBanner,
  getAllRecommend,
  getDetailProduct,
  updateAddress,
  updateBanner,
  getAllOrders,
  getOrderDetail,
  approveStatusOrder,
  rejectStatusOrder,
  getDailyOrders,
  getStockById,
  updateStockIn,
  getAllStock,
  getLotIdByProduct,
  stockOutWhenBuy,
  updateBigstock,
  insertStockOut,
  getStockOutByProduct,
  getStockInByProduct,
  getFillterOrders,
  getAllOrdersDeliver,
  deliverStatusOrder
} = require("./../controller/product");

// auth
router.post("/CheckSignUp", checkSignup);
router.post("/AddUser", InsertUser);

// product
router.get("/getAllProduct", getAllProduct);
router.post("/getDetailProduct", getDetailProduct);
router.get("/getAllBanner", getAllBanner);
router.post("/inSertProduct", inSertProduct);
router.post("/productUpload", productUpload);
router.post("/bannerUpload", requireSignin, bannerUpload);
router.post("/addRecommend", addRecommend);
router.get("/getAllRecommend", getAllRecommend);
router.post("/deleteRecommend", deleteRecommend);
router.put("/updateProduct", auth, requireSignin, updateProduct);
router.put("/updateStock", updateStock);
router.get("/getImage/:product/:image", getImage);
router.post("/addOrder", addOrder);
router.post("/getBanner", getBanner);
router.put("/updateAddress", updateAddress);

//admin
router.post(
  "/adminSignup",
  [
    body("email").isEmail().trim(),
    body("phone").isMobilePhone(),
    body("password")
      .isString()
      .isLength({ min: 8 })
      .withMessage("min 8")
      .isLength({ max: 20 })
      .withMessage("max 20")
      .isAlphanumeric()
      .withMessage("Please enter password with text and number"),
    body("confirmPassword").custom((value, { req }) => {
      if (value !== req.body.password) {
        throw new Error("Password dont match");
      }
      return true;
    }),
  ],
  adminSignup
);
router.post("/adminSignin", adminSignin);
router.post("/updateBanner", auth, requireSignin, updateBanner);
router.post("/getAllOrders", requireSignin, getAllOrders);
router.post("/getOrderDetail", auth, requireSignin, getOrderDetail);
router.post("/getDailyOrders", auth, requireSignin, getDailyOrders);

router.put("/approveOrder", auth, requireSignin, approveStatusOrder);
router.put("/rejectOrder", auth, requireSignin, rejectStatusOrder);
router.post("/updateStockIn", auth, requireSignin, updateStockIn);
router.post("/getStockById", auth, requireSignin, getStockById);
router.get("/getAllStock", auth, requireSignin, getAllStock);
router.post("/getLotById", auth, requireSignin, getLotIdByProduct);

router.post("/stockOutWhenBuy", auth, requireSignin, stockOutWhenBuy);
router.post("/updateBigstock", auth, requireSignin, updateBigstock);
router.post("/insertStockOut", auth, requireSignin, insertStockOut);
router.post("/getStockInByProduct", auth, requireSignin, getStockInByProduct);
router.post("/getStockOutByProduct", auth, requireSignin, getStockOutByProduct);
router.post("/getFillterOrders", auth, requireSignin, getFillterOrders);
router.post("/getAllOrdersDeliver", auth, requireSignin, getAllOrdersDeliver);
router.post("/deliverStatusOrder", auth, requireSignin, deliverStatusOrder);

// insertStockOut
// getDailyOrders
// updateBigstock
module.exports = router;

