const jwt = require("jsonwebtoken");

const config = require("./../config");

const verifyToken = (req, res, next) => {
  const bearerHeader =
    req.body.token || req.query.token || req.headers["authorization"];


  const bearer = bearerHeader.split(" ");
  const bearerToken = bearer[1];


  if (!bearerHeader) {
    return res.status(403).send({
      success: "ERROR",
      message: "A token is required for authentication",
    });
  }
  try {
    const decoded = jwt.verify(bearerToken, config.secret);
    req.user = decoded;
  } catch (err) {
    return res.status(401).send({
      success: "ERROR",
      message: "Invalid Token",
    });
  }
  return next();
};

module.exports = verifyToken;
