const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const myConnection = require("express-myconnection");
require("dotenv").config();
const dbOption = require("./config");
const fs = require("fs");
var moment = require("moment-timezone");
const cron = require("node-cron");
const router = require("./routes/index");
const PORT = 8004;

// let hex = "😀".codePointAt(0).toString(16)
// let emo = String.fromCodePoint("0x"+hex);
// console.log(hex, emo)

const corsOptions = {
  origin: "*",
  // methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  // preflightContinue: false,
  // optionsSuccessStatus: 204,
  credentials: true,
}

app.use(cors());

app.use(bodyParser.json());

app.use(express.json());

let connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
  database: process.env.DB_DATABASE,
  dateStrings: true,
  // insecureAuth: true,
  charset : 'utf8mb4'
});

connection.connect((err) => {
  if (err) {
    return console.log(err);
  }
});

app.use(myConnection(mysql, dbOption.dbOption, "pool"));

app.use("/api", router);

app.listen(PORT, () => {
  console.log("ready server on http://localhost:" + PORT);
});
