const fs = require('fs');


module.exports = {
	secret: '4r0j959709ni62dasdfkaknkndfvkwe6783993h13bjsdcksjdvkawdf392832340192ekjfbkjfa8366612553078u478o',
	dbOption: {
		host: process.env.DB_HOST,
		user: process.env.DB_USERNAME,
		password: process.env.DB_PASSWORD,
		port: process.env.DB_PORT,
		database: process.env.DB_DATABASE,
		dateStrings: true,
        dialect: 'mysql',
        logging: true,
        force: true,
        timezone: '+07:00',
		charset : 'utf8mb4'
        // sslmode = REQUIRED
		// insecureAuth: false,
	}
};

